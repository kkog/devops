variable "user" {
  type = string
}

variable "projet" {
  type = string
}

variable "subnet_cidr_block" {
  type = string
}

variable "availability_zone_suffix" {
  type = string
}

variable "number_of_instances" {
  type = number
}

variable "ingress_ports" {
  type = list(string)
}

variable "egress_ports" {
  type = list(string)
}

variable "egress_cidr_block" {
  type = list(string)
}

variable "ingress_cidr_block" {
  type = list(string)
}

variable "region" {
  type = string
}

variable "access_key" {
  type = string
}

variable "secret_key" {
  type = string
}