resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "my_ig" {
  vpc_id = aws_vpc.my_vpc.id
}

resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.my_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_ig.id

  }
}

resource "aws_subnet" "my_subnet" {
  availability_zone = local.availability_zone
  cidr_block        = var.subnet_cidr_block
  vpc_id            = aws_vpc.my_vpc.id

  tags = {
    Name   = "camara.coumba"
    Projet = "Devops"
  }
}

resource "aws_route_table_association" "my_route_table_association" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.route_table.id
}

resource "aws_security_group" "my_security_group" {
  name_prefix = "camara.coumba"
  vpc_id      = aws_vpc.my_vpc.id
  tags = {
    Name   = "camara.coumba"
    Projet = var.projet
  }

  dynamic "ingress" {
    for_each = var.ingress_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      cidr_blocks = var.ingress_cidr_block
      protocol    = "tcp"
    }
  }

  dynamic "egress" {
    for_each = var.egress_ports
    content {
      from_port   = egress.value
      to_port     = egress.value
      cidr_blocks = var.egress_cidr_block
      protocol    = "-1"
    }
  }

}


resource "aws_instance" "web" {
  ami                         = data.aws_ami.ubuntu.id
  subnet_id                   = aws_subnet.my_subnet.id
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.my_security_group.id]
  associate_public_ip_address = "true"
  key_name                    = aws_key_pair.ssh_ansible.key_name

  tags = {
    Name   = "camara.coumba"
    Projet = "Devops"
  }



  connection {
    type        = "ssh"
    host        = aws_instance.web.public_ip
    user        = "ubuntu"
    private_key = file("~/.ssh/id_rsa")
    timeout     = "4m"
  }

}

resource "aws_key_pair" "ssh_ansible" {
  key_name   = "ansible-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjHdyXC+kt0kCAZTHshFeCgM71CRD8fbfKcHU6Iul8JjNIXibxDl4QLTXokJSiI5AFucahbIlz+Rb/d0LcATgp9zjA0oNrNDqLaZb/4xzRvwwMeLE47SPKzK9dsSK5SM/fJaWUMU1/xpGVgiknF8xkHv9eWQVNcm8h5qXtIio1VlMdC4WAw588/i9SK38l9nbItsVg8DwKOXzxZDXYy1NpMuOTDQnJdxbPxAvyt/xjyDlu3TwzzIRI4pvruT5YH1e3Z/2mAqciTNSXxneGSxCIaQhivFj/ThT28GHSN8X+aMIFpc+wtThg1M6A+FNiai3A+iijc8/KYpX29sLaTl3F ubuntu@ip-10-0-1-213"

}